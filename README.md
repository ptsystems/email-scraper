# Installatie

## Stap 1
Download NPM van https://www.npmjs.com/get-npm

## Stap 2
Kopieer of hernoem het bestand .env.example naar .env en vul in dit bestand de bijbehorende e-mail gegevens in.

## Stap 3
Open een command prompt in de huidige map en voer de onderstaande commando's uit
```bash
# Deze hoeft maar 1 keer aangeroepen worden tenzij de code veranderd.
npm install

# Dit is het commando waarmee het script gestart kan worden
npm run dev
```
import * as moment from 'moment';
import { ParsedMail } from "mailparser";
import prettyBytes = require("pretty-bytes");
import { mkdir, writeFile } from 'fs';
import { promisify } from 'util';

const fs = {
  mkdir: promisify(mkdir),
  writeFile: promisify(writeFile)
};

interface Race
{
  city: string,
  code: string,
  section: string,
  latitude: string,
  longitude: string,
  pigeonCount: Number,
  startsAt: moment.Moment,
  entries: Entry[]
}

interface Entry
{
  rank: Number,
  points: Number,
  signed: Number,
  club: String,
  membership: String,
  birthYear: String,
  ringnumber: String,
  speed: Number,
  distance: Number,
  arrivedAt: moment.Moment;
}

const getRaceFromLine = (line: string): Race => ({
  city: line.substr(19, 46).trim(),
  code: line.substr(94, 3).trim(),
  latitude: line.substr(73, 10).trim(),
  longitude: line.substr(83, 10).trim(),
  section: line.substr(97, 4).trim(),
  pigeonCount: parseInt(line.substr(140, 5).trim()),
  startsAt: moment(`${line.substr(5, 12)}`, 'DDMMYYhhmm'),
  entries: []
})

const getEntryFromLine = (line: string, race: Race): Entry => ({
  rank: parseInt(line.substr(3, 5)),
  signed: parseInt(line.substr(41, 3)),
  points: parseInt(line.substr(71, 6)),
  club: line.substr(13, 4),
  membership: line.substr(17, 4),
  birthYear: line.substr(29, 2),
  ringnumber: line.substr(34, 7),
  distance: parseInt(line.substr(47, 7)),
  speed: parseInt(line.substr(54, 7)),
  arrivedAt: moment(line.substr(61, 4) + race.startsAt.year() + line.substr(65, 6), 'DDMMYYYYhhmmss')
});

const parseUDP = data => {
  let race: Race = null;

  for(let line of data.split('\n')) {
    if(line.startsWith('00005')) {
      race = getRaceFromLine(line);
      console.log(`Race ${race.code} - ${race.city} | Club: ${race.section} | Contestants: ${race.pigeonCount} | Starts at: ${race.startsAt.format('YYYY/MM/DD HH:mm')}`);
    }
    else if(line.startsWith('501')) {
      if(!race) continue;
      race.entries.push(getEntryFromLine(line, race));
    }
  }
  return race;
}

export default async (mail: ParsedMail) => {
  for(let attachment of mail.attachments) {
    const extension = attachment.filename.split('.').pop().toUpperCase();
    if(extension === 'UDP') {
      const race = parseUDP(attachment.content.toString());
      const dir = `races/${race.startsAt.format('YYYY')}`;
      await fs.mkdir(dir, { recursive: true });
      await fs.writeFile(`${dir}/${race.code}-${race.city}-${race.section}.json`, JSON.stringify(race, null, 4));
    } else {
      console.log('Extension of attachment is not recognized (%s - %s)', attachment.filename, prettyBytes(attachment.size))
    }
  }
}

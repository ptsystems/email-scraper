import * as dotenv from 'dotenv';
dotenv.config()

import imap from "./imap";
import * as prettyBytes from 'pretty-bytes';
import { ParsedMail } from 'mailparser';
import parser from './parser';

const printMail = (mail: ParsedMail) => {
  const attachmentSize = mail.attachments.reduce((a, b) => a + b.size, 0)
  const limit = 512 * 1000 ** 2
  console.log(`Unread e-mail '${mail.subject}' was sent by ${mail.from.text} (${mail.attachments.length} attachments - ${prettyBytes(attachmentSize)})`);

  if(attachmentSize > limit) {
    console.log('Attachment size exceeded the 512MB limit');
  }
};

const parseUnreadMails = async () => {
  const mails = await imap.fetchUnseen('INBOX');
  for(let email of mails) {
    printMail(email);
    parser(email);
    imap.setFlags(email.uid, ['\\Seen']);
  }
  setTimeout(parseUnreadMails, 60 * 1000);
}

imap.on('ready', () => {
  parseUnreadMails();
});

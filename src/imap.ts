import * as IMAP from 'imap';
// import { promisify } from 'util';
// import { promisifyAll } from 'bluebird';
import { simpleParser, ParsedMail } from 'mailparser';


// const AsyncIMAP = promisifyAll(IMAP);

interface ParsedMessage extends ParsedMail
{
  uid?: Number;
}

class IMAPAsync extends IMAP
{
  public setFlags(no, flag) {
    return new Promise((resolve, reject) => {
      super.setFlags(no, flag, (err) => {
        if(err) return reject(err);
        resolve();
      });
    })
  }

  public search(query) {
    return new Promise((resolve, reject) => {
      super.search(query, (err, result) => {
        if(err) return reject(err);
        resolve(result);
      });
    })
  }

  public openBox(box, readOnly) {
    return new Promise((resolve, reject) => {
      super.openBox(box, readOnly, (err, result) => {
        if(err) return reject(err);
        resolve(result);
      });
    })
  }

  protected onMessage(msg): Promise<ParsedMessage> {
    return new Promise((resolve, reject) => {
      let uid: Number;
      let mail: ParsedMessage;
  
      msg.on('body', async stream => {
        mail = await simpleParser(stream) as ParsedMessage
        mail.uid = uid;
        resolve(mail);
      });
      msg.on('attributes', attributes => uid = attributes.uid);
      msg.on('error', reject);
      setTimeout(() => reject(Error('Timeout of 5000 ms has been reached.')), 5000);
    });
  }

  public fetchUnseen(box?: string) : Promise<ParsedMessage[]> {
    return new Promise(async (resolve, reject) => {
      if(box) await this.openBox(box, false);
    
      const mails = [];
      const uids = await this.search(['UNSEEN']);
      try {
        const fetch = imap.fetch(uids, {
          bodies: '',
          struct: true
        });
        fetch.on('error', err => reject(err));
        fetch.on('message', (msg, no) => mails.push(this.onMessage(msg)));
        fetch.on('end', async () => resolve(await Promise.all(mails)));
      } catch (error) {
        if(error.message === 'Nothing to fetch') return console.log(error.message);
        console.error(error);
      }
    })
  }
}


const imap = new IMAPAsync({
  tls: !!process.env.IMAP_TLS,
  host: process.env.IMAP_HOSTNAME,
  user: process.env.IMAP_USERNAME,
  password: process.env.IMAP_PASSWORD,
  port: parseInt(process.env.IMAP_PORT) || 993,
  authTimeout: parseInt(process.env.IMAP_AUTH_TIMEOUT) || 5000,
  tlsOptions: { rejectUnauthorized: false },
});

imap.connect();

export default imap;
